﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;

public class PlayerAttributes : MonoBehaviour
{
	/**
	 * 
	 * Script that deals with attributes and functions associated with player
	 *
	*/

	public int salt = 0;//bullets


	public GameObject saltUI;//salt counter on screen
	public GameObject projectilePref; //prefab of the the proectile

	public GameObject enemy;

	// Use this for initialization
	void Start ()
	{}
	
	// Update is called once per frame
	void Update () 
	{
		saltUI.GetComponent<Text> ().text = "Salt: " + salt;
		if (Input.GetKeyDown ("v") && salt > 0) //as long as they have ammo
		{
			Shoot ();
		}
	}


	public int GetSaltCount
	{
		get
		{
			return salt;
		}	
		set
		{
			salt = value;
		}
	}

	void Shoot()
	{
		GameObject projectile;
		Vector3 projectiletPos = new Vector3 (transform.position.x+2, transform.position.y+3, transform.position.z+1);//where the bullet spawns
		projectile = (Instantiate (projectilePref, projectiletPos, transform.rotation)) as GameObject; 
		projectile.GetComponent<Rigidbody> ().AddForce (transform.forward*1000); //direction and speed the bullet goes in
		salt = salt - 1;
	}

	IEnumerator OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "endPoint") 
		{
			Debug.Log ("reached end");
			enemy.transform.position = new Vector3 (110f, 0f, 185);//players current location
			yield return new WaitForSeconds (0.8f);
			transform.position = new Vector3 (0f, 5f, -4f);//start position
		}
	}

}
