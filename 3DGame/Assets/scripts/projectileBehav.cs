﻿using UnityEngine;
using System.Collections;

public class projectileBehav : MonoBehaviour
{
	/**
	 * 
	 * Script that creates a leftovers from the salt once it hits the ground
	 *
	*/
	public GameObject ashPref;

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "floor")
		{
			GameObject ash;
			Vector3 ashPos = new Vector3 (transform.position.x, 0, transform.position.z);//keeps it on the
			Quaternion ashRot = Quaternion.Euler (-90, 0, 0);//sets rotation of the new object so its facing upright
			ash = (Instantiate (ashPref, ashPos, ashRot)) as GameObject;
			Destroy (gameObject);
		}
	}
}
