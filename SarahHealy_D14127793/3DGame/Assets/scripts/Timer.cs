﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour 
{
	/**
	 * 
	 * Script that deals the countdown timer variables and its UI components
	 *
	*/
	public float timeLeft=500;
	public GameObject player;

	new AudioSource audio;


	void Start ()
	{
		GetComponent<Slider> ().maxValue = timeLeft;//sets the max value of the slider to the given amount of time to finish
		audio=GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		timeLeft -= Time.deltaTime;//counts down
		GetComponent<Slider> ().value = timeLeft; //pulls the slider down with the time

		if (timeLeft <= 0) //time up 
		{
			audio.Play();
			player.transform.position = new Vector3 (0f, 5f, -4f);//back to start position
			timeLeft = 500;//resets timer
		}
	}
		
}
