﻿using UnityEngine;
using System.Collections;

public class ghostBehaviour : MonoBehaviour 
{
	/**
	 * 
	 * Script that deals with enemy behaviour, its AI features and effects on the player
	 *
	*/

	/*all vars to do with waypoints*/
	public GameObject objectWithScript;
	WayPointsManager wayPointMang;
	GameObject currentTarget; //the waypoint enemy is travelling to
	NavMeshAgent agent;
	Vector3 direction;//direction for enemy to go

	Vector3 selfPos; // the position the enemy is at
	Vector3 targetPos; // position the next way point is at

	Timer timers; //Timer script
	public GameObject sldr;//UI countdown

	float randomX;
	float randomZ;

	/*all vars to do with player*/
	public GameObject player;
	Vector3 playerPos;

	// Use this for initialization
	void Start () 
	{	
		wayPointMang = objectWithScript.GetComponent<WayPointsManager> ();//contains waypoints array
		currentTarget = wayPointMang.NextWaypoint (null);	//starts the function looking for waypoints

		agent = GetComponent<NavMeshAgent>(); 
		direction = agent.destination; //the next waypoint to go to

		timers = sldr.GetComponent<Timer> ();

	}

	// Update is called once per frame
	void Update ()
	{
		selfPos = transform.position;
		targetPos = currentTarget.transform.position; //waypoint position
		playerPos = player.transform.position;


		if (Vector3.Distance(selfPos, playerPos)>30.0f)//patrol when not near player
		{ 
			
			direction = targetPos; //enemy needs to go towards here
			agent.destination = direction;
			if (Vector3.Distance(selfPos, targetPos)<8.0f) //moves to next when it get within range of waypoint
			{
				currentTarget = wayPointMang.NextWaypoint (currentTarget);//changes the enemys current target to the next
			}
		} 
		else//enemy is beside player
		{
			direction = playerPos;
			agent.destination = direction;
		}
			
	}
		

	void OnTriggerEnter(Collider col)
	{
		randomX=Random.Range(0f, 120f);
		randomZ=Random.Range(0f, 185f);

		Vector3 teleport = new Vector3(randomX, 5f, randomZ); //random point on the maps

		if (col.gameObject.tag == "Player") 
		{
			timers.timeLeft = timers.timeLeft - 20;//takes away time left to escape
			transform.position=teleport; //moves the enemy
		}
		if (col.gameObject.tag == "projectile") 
		{
			transform.position=teleport;
		}
	}
		

			
}
