﻿using UnityEngine;
using System.Collections;

public class paperBehav : MonoBehaviour
{
	/**
	 * 
	 * Script that deals with the collectable papers scatterd around the game
	 *
	*/

	/*shows messages when found*/
	public GameObject note;
	bool besideNote;

	public GameObject collectedNote; //copy of the note you can check in pause menu
	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown ("q") && besideNote)//can only pick up if inside trigger
		{
			note.SetActive (!note.activeSelf);//show note
			collectedNote.SetActive(true);//add note to collection
		} 
		if(besideNote==false)
		{
			note.SetActive (false);
		}
	}

	void OnTriggerEnter()
	{
		besideNote = true;
	}
	void OnTriggerExit()
	{
		besideNote = false;
	}
}
