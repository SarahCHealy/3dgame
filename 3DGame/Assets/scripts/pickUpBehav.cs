﻿using UnityEngine;
using System.Collections;

public class pickUpBehav : MonoBehaviour 
{
	/**
	 * 
	 * Script that deals with ammo pick ups around the game
	 *
	*/

	PlayerAttributes attributes;
	public GameObject player;

	bool nearPickUp;

	void Start () 
	{
		
		attributes = player.GetComponent<PlayerAttributes> ();

	}
	void Update () 
	{
		if (Input.GetKeyDown ("q") && nearPickUp)//can only pick up if inside trigger
		{
			attributes.salt += 1;
			Destroy (gameObject);
		}

	}

	void OnTriggerEnter(Collider col)
	{

		if (col.gameObject.tag == "Player")
		{
			nearPickUp = true;
		}
	}
	void OnTriggerExit(Collider col)
	{

		if (col.gameObject.tag == "Player")
		{
			nearPickUp = false;
		}
	}




}

