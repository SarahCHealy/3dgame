﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;

public class analyticsScript : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		Analytics.CustomEvent ("startingPoint", new Dictionary <string, object>
			{
				{"ObjectName", gameObject.name},
				{"xPos", transform.position.x},
				{"yPos", transform.position.y},
				{"zPos", transform.position.z}
			});
	}
	
	// Update is called once per frame
	void Update () {}
}
