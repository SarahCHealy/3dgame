﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class tutorial : MonoBehaviour
{
	/**
	 * 
	 * Script that shows tutorial text boxes on screen when triggered
	 *
	*/

	public GameObject tutUI;
	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}

	void OnTriggerEnter()
	{
		tutUI.SetActive (true);
	}
	void OnTriggerExit()
	{
		tutUI.SetActive (false);
		Destroy (gameObject);
	}
}
