﻿using UnityEngine;
using System.Collections;

public class PauseScreen : MonoBehaviour 
{
	/**
	 * 
	 * Script that deals functions when the game is on the pause screen menu as well as the collectable items in the game
	 *
	*/

	bool isPaused;
	public GameObject pauseMenu;//contains pause screen options
	startScreen gameStart; //script wth start screen functions

	/*camera stuff*/
	public GameObject player;
	public GameObject cam;//scene cam not attached to player


	/*show collected itmes stuff*/
	public GameObject textUI;//paused title
	public GameObject quitButton;//quit button
	public GameObject itemCollection;//canvas that holds pictures of items collected
	public GameObject itemsButton;//button to show collection


	bool onItemScreen;
	// Use this for initialization
	void Start () 
	{
		isPaused = false;
		gameStart = GetComponent<startScreen> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		/*starts pause menu*/
		if (Input.GetKeyDown("escape")&& gameStart.gameStarted && !onItemScreen) //not on start screen
		{
			isPaused = !isPaused;
			pauseMenu.SetActive (isPaused);//activates pause canvas
		}
		if (isPaused)//freezes game no movement happens
		{
			Time.timeScale = 0;
			Cursor.visible = true; //shows arrow on screen
			cam.SetActive (true); //changes cam so mouse can click on buttons
			player.SetActive (false);
		} 
		else if(isPaused==false && gameStart.gameStarted)
		{
			Time.timeScale = 1;
			Cursor.visible = false;
			cam.SetActive (false);
			player.SetActive (true);//changes camera back to player
			itemCollection.SetActive (false);//makes sure  item collection canvas off
		}
	}

	public void ShowPapersCollected()//used when items collected is clicked
	{
		textUI.SetActive (false);
		quitButton.SetActive (false);
		itemCollection.SetActive (true);//object where the item UI images are stored
		itemsButton.SetActive (false);
		onItemScreen=true;
	}

	public void BackToPauseScreen()//used when back button on papersCanvasclicked
	{
		textUI.SetActive (true);
		quitButton.SetActive (true);
		itemCollection.SetActive (false);
		itemsButton.SetActive (true);
		onItemScreen=false;
	}



}
