﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class lightBehaviour : MonoBehaviour
{
	Light flickeringLight; //light component of lights
	new AudioSource audio;
	// Use this for initialization
	void Start () 
	{
		flickeringLight = GetComponent<Light> ();
		audio = GetComponent<AudioSource> ();
		InvokeRepeating ("FlickerLight", 0f, 0.09f); //repeats flickerLight at different times
	}
	
	// Update is called once per frame
	void Update () 
	{}
		
	void FlickerLight()
	{
		
		flickeringLight.enabled=!flickeringLight.enabled;
		audio.enabled = !audio.enabled;
	}
		
}
