﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

[Serializable]
public struct GameStatus
{
	public Vector3 playerPos;
	public float timeRemaining;
	public int saltCount;
	public bool note1; 
	public bool note2;
	public bool note3;
	public bool note4; 
	public bool note5; 
	public bool note6;
	public bool note7; 
	public bool note8; 
	public bool soundOff;
	public bool musicOff;
	public float musicVolume;
}

public class startScreen : MonoBehaviour 
{
	/**
	 * 
	 * Script that deals functions to do with the start screen as well as saving functions
	 *
	*/
	public GameObject startMenu; //canvas
	public bool gameStarted;//if the game is on pause screen

	/*AUDIO STUFF*/
	public AudioMixer masterMix; //master mixer
	const float minVol=-80f;
	const float maxVol=0f;

	public GameObject volSldr;//will higher or lower vol
	float val;

	/*camera stuff*/
	public GameObject player;//will have control of camera during game
	public GameObject cam;//camera for start and pause screen

	/*scene set up stuff*/
	public GameObject pickUpPref; //prefab for ammo pick ups
	int amountOfPickUps;//amount of pickups around game

	/*Save stuff*/
	public Vector3 lastPlayerPos; //where the player will lastStopped
	PlayerAttributes attributes; //needed to get salt;
	public int saltLeft; //how much salt they have left
	public GameObject timeSlider;//where timer script is attached 
	Timer timeGone; //needed to get timeLeft
	public GameObject note1; //notes player has found
	public GameObject note2; //notes player has found
	public GameObject note3; //notes player has found
	public GameObject note4; //notes player has found
	public GameObject note5; //notes player has found
	public GameObject note6; //notes player has found
	public GameObject note7; //notes player has found
	public GameObject note8; //notes player has found
	public GameObject soundToggle;
	public GameObject musicToggle;
	public float musicVol;
	GameStatus gameStat; //data to save
	string filePath; 
	string fileName = "3DGameSaveData.json";



	// Use this for initialization
	void Start () 
	{
		startMenu.SetActive (true);
		gameStarted=false;
		val = volSldr.GetComponent<Slider> ().value;
		masterMix.SetFloat ("masterVol", val);
		amountOfPickUps = 10;
		ScatterPickUps ();
		timeGone =timeSlider.GetComponent<Timer> ();
		attributes = player.GetComponent<PlayerAttributes> ();

		filePath = Application.persistentDataPath;
		gameStat = new GameStatus ();
		LoadData ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gameStarted == false) 
		{
			Time.timeScale = 0; //game start off with nothing moving
			Cursor.visible = true;
		}

	}

	public void startGame()//will be called when start button is clicked
	{
		Time.timeScale = 1;
		startMenu.SetActive (false);
		gameStarted=true;//hides start screen
		Cursor.visible = false;
		cam.SetActive (false);
		player.SetActive (true);
	}

	public void muteFx(bool val)//attached to fx toggle will turn fx sounds on and off
	{
		if (val) //box ticked
		{
			masterMix.SetFloat ("fxVol", maxVol);
		}
		else
		{
			masterMix.SetFloat ("fxVol", minVol);
		}
	}

	public void muteMusic(bool val)//attached to music toggle will turn music sounds on and off
	{
		if (val) //box ticked
		{
			masterMix.SetFloat ("sndscapeVol", maxVol);
		}
		else
		{
			masterMix.SetFloat ("sndscapeVol", minVol);
		}
	}

	public void changeVol(float val)//attached to sound slider will control master audiomixer
	{
		val = volSldr.GetComponent<Slider> ().value;
		masterMix.SetFloat ("masterVol", val);
	}

	void ScatterPickUps()//will spawn picks ups in different locations averytime game starts
	{

		for(int i=amountOfPickUps; i>=0; i--)
		{
			float randomX=UnityEngine.Random.Range(0.0f, 115f);//random x position
			float randomZ=UnityEngine.Random.Range(0.0f, 185f);//random z positions
			GameObject pickUp;
			Vector3 pickUpPos = new Vector3 (randomX, 0.5f, randomZ);
			pickUp = (Instantiate (pickUpPref, pickUpPos, transform.rotation)) as GameObject;
		}
	}


	/*will be called when quit button in menu screen is clicked*/
	public void QuitGame()
	{
		/*data that needs to be saved*/
		gameStat.playerPos = player.transform.position;
		gameStat.timeRemaining = timeGone.timeLeft;
		gameStat.saltCount = attributes.salt;
		gameStat.note1 = note1.activeSelf;
		gameStat.note2 = note2.activeSelf;
		gameStat.note3 = note3.activeSelf;
		gameStat.note4 = note4.activeSelf;
		gameStat.note5 = note5.activeSelf;
		gameStat.note6 = note6.activeSelf;
		gameStat.note7 = note7.activeSelf;
		gameStat.note8 = note8.activeSelf;
		gameStat.soundOff = soundToggle.GetComponent<Toggle>().isOn;
		gameStat.musicOff = musicToggle.GetComponent<Toggle>().isOn;
		gameStat.musicVolume = volSldr.GetComponent<Slider> ().value;

		String gameStatJson = JsonUtility.ToJson (gameStat); 
		File.WriteAllText (filePath + "/" + fileName, gameStatJson); //saves the data to the external file
		Debug.Log ("saved");

		Application.Quit ();
	}

	/*called when start button clicked*/
	public void LoadData()
	{
		/*load previously saved*/
		if (File.Exists (filePath + "/" + fileName))
		{
			String loadedJson = File.ReadAllText (filePath + "/" + fileName);
			gameStat = JsonUtility.FromJson<GameStatus> (loadedJson);
			player.transform.position = gameStat.playerPos;
			timeGone.timeLeft = gameStat.timeRemaining;
			attributes.salt = gameStat.saltCount;
			note1.SetActive(gameStat.note1);
			note2.SetActive(gameStat.note2);
			note3.SetActive(gameStat.note3);
			note4.SetActive(gameStat.note4);
			note5.SetActive(gameStat.note5);
			note6.SetActive(gameStat.note6);
			note7.SetActive(gameStat.note7);
			note8.SetActive(gameStat.note8);
			soundToggle.GetComponent<Toggle> ().isOn = gameStat.soundOff;
			musicToggle.GetComponent<Toggle>().isOn= gameStat.musicOff;
			volSldr.GetComponent<Slider> ().value = gameStat.musicVolume;
		}
		else//no save data found
		{
			gameStat.playerPos = player.transform.position;
			gameStat.timeRemaining = timeGone.timeLeft;
			gameStat.saltCount = attributes.salt;
			gameStat.note1 = note1.activeSelf;
			gameStat.note2 = note2.activeSelf;
			gameStat.note3 = note3.activeSelf;
			gameStat.note4 = note4.activeSelf;
			gameStat.note5 = note5.activeSelf;
			gameStat.note6 = note6.activeSelf;
			gameStat.note7 = note7.activeSelf;
			gameStat.note8 = note8.activeSelf;
			gameStat.soundOff = soundToggle.GetComponent<Toggle>().isOn;
			gameStat.musicOff = musicToggle.GetComponent<Toggle>().isOn;
			gameStat.musicVolume = volSldr.GetComponent<Slider> ().value;
		}
	}


	//SAVING FUNCTIONS MODIFIED FROM LECTURE NOTES
}


