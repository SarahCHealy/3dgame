﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class DoorBehaviour : MonoBehaviour 
{
	/**
	 * 
	 * Script that allows players to open and close the doors
	 *
	*/

	float smooth =2.0f; //speed the door opens
	float angleForOpen =90.0f;

	bool open; //door is already open
	bool enter; //when player is inside trigger

	Vector3 defaultRot; //rotation door closed
	Vector3 openRot; // rotation of door open

	new AudioSource audio;
	public AudioClip [] doorNoises;

	// Use this for initialization
	void Start ()
	{
		defaultRot = transform.eulerAngles; //sets default to closed position of doors
		openRot = new Vector3 (defaultRot.x, defaultRot.y + angleForOpen, defaultRot.z); 
		audio = GetComponent<AudioSource> ();
		open = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (open)//e pressed
		{
			transform.eulerAngles = Vector3.Slerp (transform.eulerAngles, openRot, Time.deltaTime * smooth);//changes rotation to openRot smoothly

		}

		if (enter == false) //leave the area of the door
		{
			transform.eulerAngles = Vector3.Slerp (transform.eulerAngles, defaultRot, Time.deltaTime * smooth);
		}
		if (Input.GetKeyDown ("e")&&enter)//can only open door if inside trigger
		{
			open = true;
			audio.clip = doorNoises [0];
			audio.Play();
		}
	
	}

	void OnTriggerEnter(Collider coll)//move near the door
	{
		if (coll.gameObject.tag == "Player")
		{
			enter = true;
		}
	}

	void OnTriggerExit(Collider coll)//moved away from the door
	{

		if (coll.gameObject.tag == "Player")
		{
			enter = false;
			open = false;
		}
	}

	//MODIFED FROM YOUTUBE TUTORIAL
}
