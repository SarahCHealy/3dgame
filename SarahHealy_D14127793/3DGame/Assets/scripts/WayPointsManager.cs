﻿using UnityEngine;
using System.Collections;

public class WayPointsManager : MonoBehaviour 
{
	/**
	 * 
	 * Script that deals with the waypoints in the game and chaging the target for the enemy to move to
	 *
	*/
	public GameObject[] waypoints; //hold the way points
	int nextPoint; //next point to go to






	public GameObject NextWaypoint (GameObject curr) 
	{
		if (curr != null)// waypoint is there
		{
			for (int i = 0; i < waypoints.Length; i++)//go through the waypoints array
			{
				if (curr == waypoints [i]) //if waypoint passed in is equal to this position in the array
				{
					nextPoint = (i + 1) % waypoints.Length; //sets the next way point to go to and modulo sets it back to 1
				}
			}
		}
		else 
		{
			nextPoint = 0; //default position is the first in the array
		}
		return waypoints [nextPoint];//returns next waypoint in line
	}

	void Start () {}
	void Update () {}

	//MODIFED FROM LECTURE NOTES
}
