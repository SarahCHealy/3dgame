﻿using UnityEngine;
using System.Collections;

public class CollisionWithPlayer : MonoBehaviour
{
	Timer timers;
	public GameObject sldr;

	// Use this for initialization
	void Start () 
	{
		timers = sldr.GetComponent<Timer> ();
	}
	
	// Update is called once per frame
	void Update () {}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			timers.GetTimeLeft = timers.GetTimeLeft - 20;
		}
	}
}
