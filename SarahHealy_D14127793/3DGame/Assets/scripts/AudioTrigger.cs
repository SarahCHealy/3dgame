﻿using UnityEngine;
using System.Collections;

public class AudioTrigger : MonoBehaviour 
{
	/**
	 * 
	 * Script that actives audio sources around the game when players walk into triggers
	 *
	*/
	new AudioSource audio;


	// Use this for initialization
	void Start () 
	{
		audio = GetComponentInChildren<AudioSource> (); //gets component in a child object that this script is attached to
	}
	
	// Update is called once per frame
	void Update () 
	{}

	IEnumerator OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			if (audio.isPlaying == false) 
			{
				audio.Play();
				yield return new WaitForSeconds (audio.clip.length); //wait for the clip to play before destroying
				Destroy (gameObject);
			}
		}
	}
}
